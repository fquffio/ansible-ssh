SSH
===

This role's duty is to configure SSH daemon, deploy SSH keys for all users, and [Yubico](https://developers.yubico.com/) multi-factor authentication for remote shell sessions.

Variables
---------

### `ssh_key_private`
A private SSH key to be used as default key by all users in the system. If this is empty, a new key will be generated.

### `ssh_key_public`
An optional public SSH key, corresponding to `ssh_key_private`, to be used for your convenience. If `ssh_key_private` is empty, this will be generated together with the private key.

### `ssh_known_hosts`
A dictionary of names and public keys to be added to `~/.ssh/known_hosts` file. By default, no entry will be added.

### `ssh_password_auth`
A boolean value to enable or disable password authentication via SSH. This can be either `yes` or `no`. By default, this is `yes` (note that in order for Yubico PAM to work, this must be enabled, otherwise Yubico MFA will be just ignored).

### `ssh_root_login`
A configuration string to enable or disable root login via SSH. This can be either `yes`, `no`, `without-password` or `forced-commands-only`. By default, this is `without-password` (that means: root login is allowed, but password authentication is disabled, thus only login via public key is permitted).

**Note**: due to how YAML parser treats `yes` and `no`, you **MUST** enclose such strings in double or single quotes, otherwise they will be interpreted as booleans.

### `ssh_yubico_id`
You can use this setting to configure the Yubico API ID to be used for OTP validation. If this is set to a value that evaluates as `false` (e.g. `0` or `no`), the Yubico PAM will be disabled. By default, this is `no` (thus, Yubico PAM is disabled).

### `ssh_yubico_keys`
A dictionary, where to each users in the system, corresponds a list of authorized YubiKeys to be allowed on login via SSH. By default, this is empty.

Example
-------

```yaml
---
ssh_key_private: "{{ lookup('file', '/Users/paolo/.ssh/id_rsa_myserver') }}"
ssh_key_public: "{{ lookup('file', '/Users/paolo/.ssh/id_rsa_myserver.pub') }}"
ssh_known_hosts:
  github.com: ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==

ssh_password_auth: yes
ssh_root_login: "no"

ssh_yubico_id: 16
ssh_yubico_keys:
  paolo:
    - cccccceviddg
```
